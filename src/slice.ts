const slice2 = (word : string,start : number,end? : number ) : string => {
	let result = word[start-1];
	for (let i = start ; i < (end || word.length); i++ ) {
		result += word[i]; 
	}
	return result;
} 

export default slice2;