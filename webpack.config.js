const path = require('path');
module.exports = {
	mode: 'development',
	devtool: "inline-source-map",
	entry: path.resolve(__dirname, 'src', 'index.ts'),
	output: {
	  path: path.resolve(__dirname, 'public'),
	  filename: 'bundle.js',
	},
	devServer: {
		static: {
		  directory: path.join(__dirname, 'public'),
		},
		compress: true,
		port: 3000,
	  },
	  resolve: {
		// Add `.ts` and `.tsx` as a resolvable extension.
		extensions: [".ts", ".tsx", "jsx", ".js"],
		// Add support for TypeScripts fully qualified ESM imports.
		extensionAlias: {
		 ".js": [".js", ".ts"],
		 ".cjs": [".cjs", ".cts"],
		 ".mjs": [".mjs", ".mts"]
		}
	  },
	  module: {
		rules: [
		  // all files with a `.ts`, `.cts`, `.mts` or `.tsx` extension will be handled by `ts-loader`
		  { test: /\.([cm]?ts|tsx)$/, loader: "ts-loader", exclude : /node_modules/ }
		]
	  }
  };